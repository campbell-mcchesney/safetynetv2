﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Data.SQLite;


// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace SafetyNetv2
{
    public sealed partial class AdminSettingsPanel : UserControl
    {
        public System.Collections.ObjectModel.ObservableCollection<Users> people = new System.Collections.ObjectModel.ObservableCollection<Users>();
        public AdminSettingsPanel()
        {
            InitializePeople();
            this.InitializeComponent();
            if (App.adminSettings.listsEnabled == 0)
            {
                ListsOnOff.IsOn = false;
            }

            if (App.adminSettings.listsEnabled == 1)
            {
                ListsOnOff.IsOn = true;
            }

            if (App.adminSettings.toggleStateBL == 0)
            {
                SiteListsTitle.Text = "Site Block List";
                SiteLists.ItemsSource = App.blockLists;
                SiteLists.DisplayMemberPath = "urlBlockItem";
                SiteListToggle.IsChecked = false;
            }

            else
            {
                SiteListsTitle.Text = "Site Allow List";
                SiteLists.ItemsSource = App.allowLists;
                SiteLists.DisplayMemberPath = "urlAllowItem";
                SiteListToggle.IsChecked = true;
            }

            AdminSettingsGrid.Visibility = Visibility.Collapsed;
            PassPromptText.Visibility = Visibility.Visible;
            PassEntry.Visibility = Visibility.Visible;
            AdminPass_Btn.Visibility = Visibility.Visible;
            
        }

        private void Collection_Changed(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }

        private void PasswordView_Loaded(object sender, RoutedEventArgs e)
        {
            PassEntry.Focus(FocusState.Programmatic);
        }

        private void InitializePeople()
        {
            for (int i = 0; i < App.person.Count; i++)
            {
                people.Add(new Users(App.person[i].userName));
            }
        }

        private void AdminSettingsGrid_Loaded(object sender, RoutedEventArgs e)
        {
            UserAction.Visibility = Visibility.Collapsed;
            UserDelete.Visibility = Visibility.Collapsed;
            AdminPass.Password = App.adminSettings.password;
            AdminEmail.Text = App.adminSettings.email;
            HomePage.Text = App.adminSettings.home;
            ReportsGrid.Visibility = Visibility.Collapsed;
        }   
        
        private void AdminEmail_TextChanged(object sender, TextChangedEventArgs e)
        {
            App.adminSettings.email = AdminEmail.Text;
            App.connection.Open();
            SQLiteCommand writeAdmin = new SQLiteCommand("UPDATE AdminSettings SET Email = @email WHERE ID = 1;", App.connection);
            writeAdmin.Parameters.AddWithValue("@email", App.adminSettings.email);
            writeAdmin.ExecuteNonQuery();
            App.connection.Close();
        }

        private void HomePage_TextChanged(object sender, TextChangedEventArgs e)
        {
            App.adminSettings.home = HomePage.Text;
            App.connection.Open();
            SQLiteCommand writeAdmin = new SQLiteCommand("UPDATE AdminSettings SET Home = @home WHERE ID = 1;", App.connection);
            writeAdmin.Parameters.AddWithValue("@home", App.adminSettings.home);
            writeAdmin.ExecuteNonQuery();
            App.connection.Close();
        }

        private void AdminPass_TextChanged(object sender, RoutedEventArgs e)
        {
            App.adminSettings.password = AdminPass.Password;
            App.connection.Open();
            SQLiteCommand writeAdmin = new SQLiteCommand("UPDATE AdminSettings SET Password = @password WHERE ID = 1;", App.connection);
            writeAdmin.Parameters.AddWithValue("@password", App.adminSettings.password);
            writeAdmin.ExecuteNonQuery();
            App.connection.Close();
        }

        private void UserSelect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            App.currentUserIndex = UserSelect.SelectedIndex;
            try
            {
                UserNameBox.Text = App.person[App.currentUserIndex].userName;
            }

            catch
            {
                UserNameBox.Text = "";
            }

            try
            {
                UserPassBox.Text = App.person[App.currentUserIndex].userPassword;
            }

            catch
            {
                UserPassBox.Text = "";
            };

            try
            {
                AgeSelect.SelectedIndex = App.person[App.currentUserIndex].userAge;
            }

            catch
            {
                AgeSelect.SelectedIndex = 0;
            };

            try
            {
                if (App.person[App.currentUserIndex].userEnabled == 1)
                {
                    UserEnableToggle.IsOn = true;
                }
                else
                {
                    UserEnableToggle.IsOn = false;
                }
            }

            catch
            {
                UserEnableToggle.IsOn = false;
            }

            if (UserNameBox.Text == "")
            {

            }

            else if (UserNameBox.Text == "New User")
            {
                UserAction.Visibility = Visibility.Visible;
                UserDelete.Visibility = Visibility.Collapsed;
                UserAction.Content = "Add";
            }

            else
            {
                UserAction.Visibility = Visibility.Visible;
                UserDelete.Visibility = Visibility.Visible;
                UserAction.Content = "Update";
            }
        }

        private void UserAction_Click(object sender, RoutedEventArgs e)
        {
            if (UserNameBox.Text == "New User")
            {
                NewUserError();
            }

            else
            {
                if (UserAction.Content.ToString() == "Add")
                {
                    App.person.Add(new Person(UserNameBox.Text, AgeSelect.SelectedIndex, UserPassBox.Text, new Uri("ms-appx:///Assets/Graphics/UserImages/guest.png"), 1));
                    people.Add(new Users(UserNameBox.Text));
                    people.CollectionChanged += Collection_Changed;
                    App.connection.Open();
                    SQLiteCommand writeUser = new SQLiteCommand("INSERT into Person(Name, UserAge, UserPass, URI, Enabled) Values(@name, @age, @pass, @uri, @state)", App.connection);
                    writeUser.Parameters.AddWithValue("@name", UserNameBox.Text);
                    writeUser.Parameters.AddWithValue("@age", AgeSelect.SelectedIndex);
                    writeUser.Parameters.AddWithValue("@pass", UserPassBox.Text);
                    writeUser.Parameters.AddWithValue("@uri", "ms-appx:///Assets/Graphics/UserImages/guest.png");
                    writeUser.Parameters.AddWithValue("@state", 1);
                    writeUser.ExecuteNonQuery();
                    App.connection.Close();
                    UserNameBox.Text = "";
                    UserPassBox.Text = "";
                    AgeSelect.SelectedIndex = 0;
                    UserEnableToggle.IsOn = false;
                }
                else
                {
                    string TempName = UserNameBox.Text;
                    App.person[App.currentUserIndex].userName = UserNameBox.Text;
                    App.person[App.currentUserIndex].userAge = AgeSelect.SelectedIndex;
                    App.person[App.currentUserIndex].userPassword = UserPassBox.Text;                    
                    App.person[App.currentUserIndex].userEnabled = App.tempUserState;
                    App.connection.Open();
                    SQLiteCommand updatePerson = new SQLiteCommand("UPDATE Person SET Name=@name , UserAge=@userage, UserPass=@userpass, URI=@uri, Enabled=@enabled WHERE Name = @tempname", App.connection);
                    updatePerson.Parameters.AddWithValue("@tempname", TempName);
                    updatePerson.Parameters.AddWithValue("@name", App.person[App.currentUserIndex].userName);
                    updatePerson.Parameters.AddWithValue("@userage", App.person[App.currentUserIndex].userAge);
                    updatePerson.Parameters.AddWithValue("@userpass", App.person[App.currentUserIndex].userPassword);
                    updatePerson.Parameters.AddWithValue("@uri", App.person[App.currentUserIndex].userPic);
                    updatePerson.Parameters.AddWithValue("@enabled", App.person[App.currentUserIndex].userEnabled);
                    updatePerson.ExecuteNonQuery();
                    App.connection.Close();
                }   
            }
        }

        private async void NewUserError()
        {
            ContentDialog UserNameError = new ContentDialog()
            {
                Title = "New User is a reserved name",
                Content = "You must change the user name to another name",
                CloseButtonText = "OK"
            };

            await UserNameError.ShowAsync();
        }

        private void UserDelete_Click(object sender, RoutedEventArgs e)
        {
            int currentUserIndexTemp = App.currentUserIndex;
            App.connection.Open();
            SQLiteCommand deleteUser = new SQLiteCommand("DELETE FROM Person WHERE Name = @name;", App.connection);
            deleteUser.Parameters.AddWithValue("@name", App.person[currentUserIndexTemp].userName);
            deleteUser.ExecuteNonQuery();
            App.connection.Close();
            App.person.RemoveAt(currentUserIndexTemp);
            people.RemoveAt(currentUserIndexTemp);
            App.currentUserIndex = 0;
            people.CollectionChanged += Collection_Changed;
            UserSelect.SelectedIndex = -1;
            UserAction.Visibility = Visibility.Collapsed;
            UserDelete.Visibility = Visibility.Collapsed;
            
        }

        private void AdminSettingsGrid_Unloaded(object sender, RoutedEventArgs e)
        {
            UserNameBox.Text = "";
            UserPassBox.Text = "";
            UserSelect.SelectedIndex = -1;
        }

        private void SiteListToggle_Checked(object sender, RoutedEventArgs e)
        {
            SiteListsTitle.Text = "Site Allow List";
            SiteLists.ItemsSource = App.allowLists;
            SiteLists.DisplayMemberPath = "urlAllowItem";
            App.adminSettings.toggleStateBL = 1;
            App.connection.Open();
            SQLiteCommand writeBAToggle = new SQLiteCommand("UPDATE AdminSettings SET ToggleStateBA = @state WHERE ID = 1;", App.connection);
            writeBAToggle.Parameters.AddWithValue("@state", App.adminSettings.toggleStateBL);
            writeBAToggle.ExecuteNonQuery();
            App.connection.Close();
        }

        private void SiteListToggle_Unchecked(object sender, RoutedEventArgs e)
        {
            SiteListsTitle.Text = "Site Block List";
            SiteLists.ItemsSource = App.blockLists;
            SiteLists.DisplayMemberPath = "urlBlockItem";
            SiteListToggle.IsChecked = false;
            App.adminSettings.toggleStateBL = 0;
            App.connection.Open();
            SQLiteCommand writeBAToggle = new SQLiteCommand("UPDATE AdminSettings SET ToggleStateBA = @state WHERE ID = 1;", App.connection);
            writeBAToggle.Parameters.AddWithValue("@state", App.adminSettings.toggleStateBL);
            writeBAToggle.ExecuteNonQuery();
            App.connection.Close();
        }

        private void ListsOnOff_Toggled(object sender, RoutedEventArgs e)
        {
            if (ListsOnOff.IsOn == true)
            {
                App.adminSettings.listsEnabled = 1;
                App.connection.Open();
                SQLiteCommand writeListEnabled = new SQLiteCommand("UPDATE AdminSettings SET ListsEnabled = @state WHERE ID = 1;", App.connection);
                writeListEnabled.Parameters.AddWithValue("@state", App.adminSettings.listsEnabled);
                writeListEnabled.ExecuteNonQuery();
                App.connection.Close();
            }

            if (ListsOnOff.IsOn == false)
            {
                App.adminSettings.listsEnabled = 0;
                App.connection.Open();
                SQLiteCommand writeListEnabled = new SQLiteCommand("UPDATE AdminSettings SET ListsEnabled = @state WHERE ID = 1;", App.connection);
                writeListEnabled.Parameters.AddWithValue("@state", App.adminSettings.listsEnabled);
                writeListEnabled.ExecuteNonQuery();
                App.connection.Close();
            }
        }

        private void ListAdd_Click(object sender, RoutedEventArgs e)
        {
            if (AddListItem.Text != "")
            {
                if (App.adminSettings.toggleStateBL == 0)
                {
                    App.blockLists.Add(new GlobalBlockList(AddListItem.Text));
                    App.blockLists.CollectionChanged += Collection_Changed;
                    App.connection.Open();
                    SQLiteCommand writeBlockList = new SQLiteCommand("INSERT into BlockList(SiteURL) Values(@URL)", App.connection);
                    writeBlockList.Parameters.AddWithValue("@URL", AddListItem.Text);
                    writeBlockList.ExecuteNonQuery();
                    App.connection.Close();
                    AddListItem.Text = "";
                }

                else
                {
                    App.allowLists.Add(new GlobalAllowList(AddListItem.Text));
                    App.allowLists.CollectionChanged += Collection_Changed;
                    App.connection.Open();
                    SQLiteCommand writeAllowList = new SQLiteCommand("INSERT into AllowList(SiteURL) Values(@URL)", App.connection);
                    writeAllowList.Parameters.AddWithValue("@URL", AddListItem.Text);
                    writeAllowList.ExecuteNonQuery();
                    App.connection.Close();
                    AddListItem.Text = "";
                }
            }

            else
            {
                NoTextErrorPopup();
            }
        }

        private async void NoTextErrorPopup()
        {
            ContentDialog NoTextError = new ContentDialog()
            {
                Title = "No Text Entered",
                Content = "Text must be entered to complete this operation",
                CloseButtonText = "OK"
            };
            await NoTextError.ShowAsync();
        }

        private void ListDelete_Click(object sender, RoutedEventArgs e)
        {
            if (App.adminSettings.toggleStateBL ==0)
            {
                App.connection.Open();
                SQLiteCommand deleteBlockItem = new SQLiteCommand("DELETE FROM BlockList WHERE SiteURL = @URL;", App.connection);
                deleteBlockItem.Parameters.AddWithValue("@URL", App.blockLists[App.BlockAllowlistIndex].urlBlockItem);
                deleteBlockItem.ExecuteNonQuery();
                App.connection.Close();
                App.blockLists.RemoveAt(App.BlockAllowlistIndex);
                App.blockLists.CollectionChanged += Collection_Changed;
                
            }

            else
            {
                App.connection.Open();
                SQLiteCommand deleteAllowItem = new SQLiteCommand("DELETE FROM AllowList WHERE SiteURL = @URL;", App.connection);
                deleteAllowItem.Parameters.AddWithValue("@URL", App.allowLists[App.BlockAllowlistIndex].urlAllowItem);
                deleteAllowItem.ExecuteNonQuery();
                App.connection.Close();
                App.allowLists.RemoveAt(App.BlockAllowlistIndex);
                App.allowLists.CollectionChanged += Collection_Changed;
            }
        }

        private void SiteLists_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            App.BlockAllowlistIndex = SiteLists.SelectedIndex;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(PassEntry.Password.ToString() == App.adminSettings.password)
            {
                AdminSettingsGrid.Visibility = Visibility.Visible;
                PassEntry.Visibility = Visibility.Collapsed;
                PassPromptText.Visibility = Visibility.Collapsed;
                AdminPass_Btn.Visibility = Visibility.Collapsed;
            }
        }

        private void PassEntry_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                try
                {
                    if (PassEntry.Password.ToString() == App.adminSettings.password)
                    {
                        AdminSettingsGrid.Visibility = Visibility.Visible;
                        PassEntry.Visibility = Visibility.Collapsed;
                        PassPromptText.Visibility = Visibility.Collapsed;
                        AdminPass_Btn.Visibility = Visibility.Collapsed;
                    }

                }
                catch (FormatException)
                {
                    // Bad address.
                }

            }
        }

        private void UserEnableToggle_Toggled(object sender, RoutedEventArgs e)
        {
            if (UserEnableToggle.IsOn == true)
            {
                App.tempUserState = 1;
            }

            if (UserEnableToggle.IsOn == false)
            {
                App.tempUserState = 0;
            }
        }

        private void Reports_Click(object sender, RoutedEventArgs e)
        {
            AdminSettingsGrid.Visibility = Visibility.Collapsed;
            ReportsGrid.Visibility = Visibility.Visible;
           
        }

        private void ReportsGrid_Loaded(object sender, RoutedEventArgs e)
        {
            UserStats.Children.Clear();
            PageLoad();
        }

        public void PageLoad()
        {
            List<ColumnDefinition> colDef = new List<ColumnDefinition>();
            RowDefinition row0 = new RowDefinition();
            RowDefinition row1 = new RowDefinition();
            RowDefinition row2 = new RowDefinition();
            DateTime today = DateTime.Now;
            DateTime sevendays = today.AddDays(-6);
            row0.MaxHeight = 40;
            row1.MaxHeight = 40;
            row2.MaxHeight = 400;
            UserStats.RowDefinitions.Add(row0);
            UserStats.RowDefinitions.Add(row1);
            UserStats.RowDefinitions.Add(row2);            
            double width = UserStats.ActualWidth / App.person.Count;
            for (int z = 0; z < App.person.Count; z++)
            {
                colDef.Add(new ColumnDefinition());
            }
            for (int i = 1; i < App.person.Count; i++)
            {
                colDef[i - 1].MinWidth = width;
                UserStats.ColumnDefinitions.Add(colDef[i - 1]);
                ListView stats = new ListView();                
                TextBlock name = new TextBlock();
                TextBlock historyTitle = new TextBlock();
                Grid.SetColumn(historyTitle, i - 1);
                Grid.SetRow(historyTitle, 1);
                Grid.SetColumn(name, i - 1);
                Grid.SetRow(name, 0);
                historyTitle.Margin = new Thickness(10);
                historyTitle.Text = "User History - Last 7 Days";
                name.Text = App.person[i].userName;
                name.Margin = new Thickness(10);

                for (int n = App.history.Count-1; n >= 0; n--)
                {
                    if (App.history[n].userName == App.person[i].userName && App.history[n].date > sevendays)
                    {
                        HyperlinkButton historyitem = new HyperlinkButton();
                        historyitem.Content = App.history[n].siteUrlHis;
                        historyitem.Foreground = new SolidColorBrush(Windows.UI.Colors.Black);
                        historyitem.Margin = new Thickness(10);
                        stats.Items.Add(historyitem);
                    }
                }
                Grid.SetColumn(stats, i - 1);
                Grid.SetRow(stats, 2);
                UserStats.Children.Add(name);
                UserStats.Children.Add(historyTitle);                
                UserStats.Children.Add(stats);                
            }

        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            AdminSettingsGrid.Visibility = Visibility.Visible;
            ReportsGrid.Visibility = Visibility.Collapsed;
        }

       
    }     
}
