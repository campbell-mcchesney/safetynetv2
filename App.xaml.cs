﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Core.Preview;
using Windows.Storage;
using System.Xml.Serialization;
using Windows.UI.Popups;
using System.Data.SQLite;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Controls.Maps;
using MailKit.Net.Smtp;

namespace SafetyNetv2
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        internal static Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
        internal static Windows.Storage.StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
        //Variables
        internal static string currentURL;
        internal static int currentUser = 0;
        internal static int currentUserIndex = 0;
        internal static int currentUserProfile = 0;
        internal static int BlockAllowlistIndex = 0;        
        internal static int tempUserState = 1;
        internal static int counter = 0;
        internal static int navtrigger = 0;
        internal static int ListsEnabled;
        internal static int blockadd = 1;
        //Collections
        internal static List<FavoritesData> favorites = new List<FavoritesData>();
        internal static List<WebHistory> history = new List<WebHistory>();
        internal static List<Users> user = new List<Users>();
        internal static List<Person> person = new List<Person>();
        internal static List<ProfilePics> profilePics = new List<ProfilePics>();
        internal static ObservableCollection<GlobalBlockList> blockLists = new ObservableCollection<GlobalBlockList>();
        internal static ObservableCollection<GlobalAllowList> allowLists = new ObservableCollection<GlobalAllowList>();
        //Objects
        internal static AdminSettingsData adminSettings = new AdminSettingsData("", "", "http://", 0, 0);
        internal static SQLiteConnection connection = new SQLiteConnection("Data Source="+localFolder.Path+"/settingsDB.db;Version=3");
        internal static SmtpClient client = new SmtpClient();
        
        public App()
        {
            this.InitializeComponent();            
            this.Suspending += OnSuspending;
            SQLiteHelp.getValues();            
        }        

            /// <summary>
            /// Invoked when the application is launched normally by the end user.  Other entry points
            /// will be used such as when the application is launched to open a specific file.
            /// </summary>
            /// <param name="e">Details about the launch request and process.</param>
            protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (e.PrelaunchActivated == false)
            {
                if (rootFrame.Content == null)
                {
                    // When the navigation stack isn't restored navigate to the first page,
                    // configuring the new page by passing required information as a navigation
                    // parameter
                    rootFrame.Navigate(typeof(MainPage), e.Arguments);
                }
                // Ensure the current window is active
                Window.Current.Activate();
            }
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }
    }

    //Classes

    public class Users
    {
        public string userName { get; set; }


        public Users(string _userName)
        {
            userName = _userName;

        }
    }

    public class Person : Users
    {
        public int userAge { get; set; }
        public string userPassword { get; set; }
        public Uri userPic { get; set; }
        public int userEnabled { get; set; }

        public Person(string _userName, int _userAge, string _userPassword, Uri _userPic, int _state) : base(_userName)
        {
            userAge = _userAge;
            userPassword = _userPassword;
            userPic = _userPic;
            userEnabled = _state;

        }
    }

    public class FavoritesData : Users
    {
        public string siteName { get; set; }
        public string siteUrl { get; set; }
        public string imageRef { get; set; }

        public FavoritesData(string _SiteName, string _SiteUrl, string _ImageRef, string _userName) : base(_userName)
        {
            userName = _userName;
            siteName = _SiteName;
            siteUrl = _SiteUrl;
            imageRef = _ImageRef;
        }
    }

    public class WebHistory : Users
    {
        public string siteUrlHis { get; set; }
        public DateTime date { get; set; }
        public int flagged { get; set; }
        public WebHistory(string _SiteName, DateTime _date, string _userName, int _flagged) : base(_userName)
        {
            userName = _userName;
            date = _date;
            siteUrlHis = _SiteName;
            flagged = _flagged;
        }
    }

    public class GlobalBlockList
    {
        public string urlBlockItem { get; set; }
        public GlobalBlockList(string _urlBlockItem)
        {
            urlBlockItem = _urlBlockItem;
        }
    }

    public class UserBlocklist : Users
    {
        public string urlBlockItem { get; set; }
        public UserBlocklist(string _UrlBlockItem, string _userName) : base(_userName)
        {
            urlBlockItem = _UrlBlockItem;
        }
    }

    public class GlobalAllowList
    {
        public string urlAllowItem { get; set; }
        public GlobalAllowList(string _urlAllowItem)
        {
            urlAllowItem = _urlAllowItem;
        }
    }

    public class UserAllowlist : Users
    {
        public string urlAllowItem { get; set; }
        public UserAllowlist(string _UrlAllowItem, string _userName) : base(_userName)
        {
            urlAllowItem = _UrlAllowItem;
        }
    }

    public class ProfilePics
    {
        public Uri profPic { get; set; }
        public ProfilePics(Uri _profPic)
        {
            profPic = _profPic;
        }
    }

    public class AdminSettingsData
    {
        public string password { get; set; } 
        public string email { get; set; }
        public  string home { get; set; } 

        public int toggleStateBL { get; set; }

        public int listsEnabled { get; set; }

        public AdminSettingsData(string _password, string _email, string _home, int _toggle, int _listsEnabled)
        {
            password = _password;
            email = _email;
            home = _home;
            toggleStateBL = _toggle;
            listsEnabled = _listsEnabled;
        }

    }

    //Database access and code for loading data into objects and lists
    public class SQLiteHelp
    {

        public static string getValues()
        {
            string text = ""; 
            
            App.connection.Open();

            SQLiteCommand selectCommandPerson = new SQLiteCommand("SELECT * from Person", App.connection);
            SQLiteDataReader queryPerson = selectCommandPerson.ExecuteReader();                
            while (queryPerson.Read())
            {
            Uri pic = new Uri(queryPerson.GetString(4));
                App.person.Add(new Person(queryPerson.GetString(1), queryPerson.GetInt32(2), queryPerson.GetString(3), pic, queryPerson.GetInt32(5)));                    
            }

            SQLiteCommand selectCommandFavorites = new SQLiteCommand("SELECT * from Favorites", App.connection);
            SQLiteDataReader queryFavorites = selectCommandFavorites.ExecuteReader();
            while (queryFavorites.Read())
            {                
                App.favorites.Add(new FavoritesData(queryFavorites.GetString(1), queryFavorites.GetString(2), queryFavorites.GetString(3), queryFavorites.GetString(4)));
            }

            SQLiteCommand selectCommandHistory = new SQLiteCommand("Select * from SiteHistory", App.connection);
            SQLiteDataReader queryHistory = selectCommandHistory.ExecuteReader();
            while (queryHistory.Read())
            {                
                DateTime date = DateTime.Parse(queryHistory.GetString(2)).Date;
                App.history.Add(new WebHistory(queryHistory.GetString(1), date, queryHistory.GetString(3), queryHistory.GetInt32(4)));
            }

            SQLiteCommand selectCommandAdmin = new SQLiteCommand("Select * from AdminSettings", App.connection);
            SQLiteDataReader queryAdmin = selectCommandAdmin.ExecuteReader();
            while (queryAdmin.Read())
            {
                App.adminSettings.password = queryAdmin.GetString(1);
                App.adminSettings.email = queryAdmin.GetString(2); 
                App.adminSettings.home = queryAdmin.GetString(3);
                App.adminSettings.toggleStateBL = queryAdmin.GetInt32(4);
                App.adminSettings.listsEnabled = queryAdmin.GetInt32(5);
            }

            SQLiteCommand selectCommandBlockList = new SQLiteCommand("Select * from BlockList", App.connection);
            SQLiteDataReader queryBlockList = selectCommandBlockList.ExecuteReader();
            while (queryBlockList.Read())
            {
                App.blockLists.Add(new GlobalBlockList(queryBlockList.GetString(1)));                
            }

            SQLiteCommand selectCommandAllowList = new SQLiteCommand("Select * from AllowList", App.connection);
            SQLiteDataReader queryAllowList = selectCommandAllowList.ExecuteReader();
            while (queryAllowList.Read())
            {
                App.allowLists.Add(new GlobalAllowList(queryAllowList.GetString(1)));
            }
            App.connection.Close();
            App.profilePics.Add(new ProfilePics(new Uri("ms-appx:///Assets/Graphics/UserImages/car.jpg")));
            App.profilePics.Add(new ProfilePics(new Uri("ms-appx:///Assets/Graphics/UserImages/Flower.jpg")));
            App.profilePics.Add(new ProfilePics(new Uri("ms-appx:///Assets/Graphics/UserImages/IronMan.jpg")));
            App.profilePics.Add(new ProfilePics(new Uri("ms-appx:///Assets/Graphics/UserImages/Jet.jpg")));
            App.profilePics.Add(new ProfilePics(new Uri("ms-appx:///Assets/Graphics/UserImages/LegoMan.jpg")));
            App.profilePics.Add(new ProfilePics(new Uri("ms-appx:///Assets/Graphics/UserImages/LegoWoman.jpg")));
            App.profilePics.Add(new ProfilePics(new Uri("ms-appx:///Assets/Graphics/UserImages/MLP.jpg")));
            App.profilePics.Add(new ProfilePics(new Uri("ms-appx:///Assets/Graphics/UserImages/Unicorn.jpg")));
            App.profilePics.Add(new ProfilePics(new Uri("ms-appx:///Assets/Graphics/UserImages/Basketball.jpg")));
            App.profilePics.Add(new ProfilePics(new Uri("ms-appx:///Assets/Graphics/UserImages/Sandcastle.jpg")));
            return text;
        }
    }
    

}
