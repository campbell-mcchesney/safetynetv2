﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using System.Xml.Serialization;
using System.Data.SQLite;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using Windows.UI.Popups;
using Windows.UI.Xaml.Media.Imaging;






// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SafetyNetv2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        
        
        public MainPage()
        {
            
            this.InitializeComponent();            
            WebPage.Visibility = Visibility.Collapsed;
            NoUser.Visibility = Visibility.Visible;
            Favorites_Btn.IsEnabled = false;
            History_Btn.IsEnabled = false;
            Settings_Btn.IsEnabled = false;
            Home_Btn.IsEnabled = false;
            Report_Btn.IsEnabled = false;
            SearchBar.Visibility = Visibility.Collapsed;
            Go.Visibility = Visibility.Collapsed;
            WebViewBack.IsEnabled = false;
            WebViewForward.IsEnabled = false;
            ArrowLeft.Visibility = Visibility.Collapsed;
            ArrowRight.Visibility = Visibility.Collapsed;
        }
        

        private void MainGrid_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
        private void Collection_Changed(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }
        private void button_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            Windows.UI.Xaml.Window.Current.CoreWindow.PointerCursor = new Windows.UI.Core.CoreCursor(Windows.UI.Core.CoreCursorType.Hand, 1);
        }
        private void button_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            Windows.UI.Xaml.Window.Current.CoreWindow.PointerCursor = new Windows.UI.Core.CoreCursor(Windows.UI.Core.CoreCursorType.Arrow, 1);
        }

        //Navigation Methods


        private void Home_Btn_Click(object sender, RoutedEventArgs e)
        {
            Uri targetUri = new Uri(App.adminSettings.home);
            WebPage.Navigate(targetUri);
        }

        //Navigate to URL bar entry
        private void Go_Click(object sender, RoutedEventArgs e)
        {
            WebNav(SearchBar.Text);
        }

        private async void Box_KeyDown(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                try
                {
                    WebNav(SearchBar.Text);
                }
                catch (FormatException)
                {
                    // Bad address.
                }

            }
        }

        //Main navigation method
        public void WebNav(string _address)
        {
            if (_address.Contains("http://"))
            {
                Uri targetUri = new Uri(_address);
                WebPage.Navigate(targetUri);
            }

            else if (_address.Contains("https://"))
            {
                Uri targetUri = new Uri(_address);
                WebPage.Navigate(targetUri);
            }

            else if (_address.Contains("http://www."))
            {
                Uri targetUri = new Uri(_address);
                WebPage.Navigate(targetUri);
            }

            else if (_address.Contains("https://www."))
            {
                Uri targetUri = new Uri(_address);
                WebPage.Navigate(targetUri);
            }

            else
            {
                string addressAlt = "http://" + _address;
                Uri targetUri = new Uri(addressAlt);
                WebPage.Navigate(targetUri);
            }
        }

        //Condition checking of block/allow list items and adding pages to history
        private void WebPage_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            
            App.currentURL = args.Uri.ToString();
            if (App.currentUser != 0)
            {
                App.history.Add(new WebHistory(App.currentURL, DateTime.Now.Date, App.person[App.currentUser].userName, 0));
                App.connection.Open();
                SQLiteCommand writeUser = new SQLiteCommand("INSERT into SiteHistory(SiteURL, Date, UserName, Flagged) Values(@siteurl, @date, @username, @flagged)", App.connection);
                writeUser.Parameters.AddWithValue("@siteurl", App.currentURL);
                writeUser.Parameters.AddWithValue("@date", DateTime.Now.Date.ToString());
                writeUser.Parameters.AddWithValue("@username", App.person[App.currentUser].userName);
                writeUser.Parameters.AddWithValue("@flagged", 0);
                writeUser.ExecuteNonQuery();
                App.connection.Close();
            }
            if (App.adminSettings.listsEnabled == 1)
            {
                if (App.adminSettings.toggleStateBL == 0)
                {
                    for (int i = 0; i < App.blockLists.Count; i++)
                    {
                        if (App.currentURL.Contains(App.blockLists[i].urlBlockItem))
                        {
                            args.Cancel = true;
                            NavagationErrorPopup();
                        }

                        else
                        {
                            SearchBar.Text = App.currentURL;
                        }
                    }
                }
                if (App.adminSettings.toggleStateBL == 1)
                {

                    for (int i = 0; i < App.allowLists.Count; i++)
                    {
                        if (App.currentURL.Contains(App.allowLists[i].urlAllowItem))
                        {
                            App.navtrigger = 1;
                        }

                        else
                        {

                        }

                    }
                    if (App.navtrigger == 1)
                    {
                        SearchBar.Text = App.currentURL;
                    }

                    if (App.navtrigger == 0)
                    {
                        args.Cancel = true;
                        NavagationErrorPopup();
                    }
                }
            }

            else
            {
                SearchBar.Text = App.currentURL;
            }
        }

        private void WebPage_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            App.navtrigger = 0;
            App.currentURL = args.Uri.ToString();
            SearchBar.Text = App.currentURL;
            
        }

        private async void NavagationErrorPopup()
        {
            ContentDialog NavError = new ContentDialog()
            {
                Title = "Blocked Page",
                Content = "This page has been blocked by the administrator",
                CloseButtonText = "OK"
            };

            await NavError.ShowAsync();
            
        }        
        
        //Report buttion actions
        private void Report_Btn_Click(object sender, RoutedEventArgs e)
        {
            
            string tempaddress = App.currentURL;
            Uri host = new Uri(tempaddress);
            string blockaddress = host.Host;
            WebPage.Visibility = Visibility.Collapsed;            
            WebNav(App.adminSettings.home);
            WebPage.Visibility = Visibility.Visible;
            FlagAddress(blockaddress, tempaddress);
            System.Threading.Timer timer = null;
            timer = new System.Threading.Timer((obj) =>
            {
                EmailAlert(tempaddress);
                timer.Dispose();
            },
                        null, 1000, System.Threading.Timeout.Infinite);            
        }

        private void FlagAddress(string _blockaddress, string _tempaddress)
        {
            if(App.blockLists.Count==0)
            {
                App.blockadd = 1;
            }
            for(int i = 0; i < App.blockLists.Count; i++)
            {
                if(App.blockLists[i].urlBlockItem.Contains(_blockaddress))
                {
                    App.blockadd = 0;
                }                
            }
            if (App.blockadd == 1)
            {
                App.blockLists.Add(new GlobalBlockList(_blockaddress));
                App.blockLists.CollectionChanged += Collection_Changed;
                App.connection.Open();
                SQLiteCommand writeBlockList = new SQLiteCommand("INSERT into BlockList(SiteURL) Values(@URL)", App.connection);
                writeBlockList.Parameters.AddWithValue("@URL", _blockaddress);
                writeBlockList.ExecuteNonQuery();
                App.connection.Close();
            }
                for (int i = 0; i < App.history.Count; i++)
                {
                    if (App.history[i].siteUrlHis.Contains(_blockaddress))
                    {
                        App.history[i].flagged = 1;
                        App.connection.Open();
                        SQLiteCommand writeHistory = new SQLiteCommand("UPDATE SiteHistory SET Flagged = @Flagged WHERE SiteURL = @SiteURL", App.connection);
                        writeHistory.Parameters.AddWithValue("@Flagged", App.history[i].flagged);
                        writeHistory.Parameters.AddWithValue("@SiteURL", App.history[i].siteUrlHis);
                        writeHistory.ExecuteNonQuery();
                        App.connection.Close();
                    }
                }            
            App.blockadd = 1;
        }        

        private void EmailAlert(string _address)
        {
            App.client.ServerCertificateValidationCallback = (s, c, h, e) => true;
            App.client.Connect("smtp.gmail.com", 587);            
            App.client.Authenticate("safetynetsystem@gmail.com", "SafeAsHouses");
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("SafetyNet", "safetynetsystem@gmail.com"));
            message.To.Add(new MailboxAddress("SafetyNet User", App.adminSettings.email));
            message.Subject = "Your child has flagged a website";
            message.Body = new TextPart("plain") { Text = @"Your child "+App.person[App.currentUser].userName+" has flagged the website "+_address+" as inappropriate.\n " +
                "Your child is requesting help and may not know how to approach you to talk about what they have experienced. We have included in this email some links to sites that " +
                "you may find helpful when discussing with your child any material or issues they have encountered. We encourage you to approach them first and equip yourself " +
                "with tools and information from the links below.\n\n" +
                "http://www.safesurfer.co.nz\n\n " +
                "https://www.netsafe.org.nz\n\n" +
                "Regards\n\n The SafetyNet Team"
            };
            App.client.Send(message);           
        }

       //User Control Menu Button Click Actions

        private void AdminSettings_Btn_Click(object sender, RoutedEventArgs e)
        {
            this.contentControlAdminSettings.Content = new AdminSettingsPanel();
        }

       

        //Code for Profiles Flyout
        private void ProfilesGrid_Loaded(object sender, RoutedEventArgs e)
        {
            ProfPass.Visibility = Visibility.Collapsed;
            ProfPassTxt.Visibility = Visibility.Collapsed;
            ProfPassExe.Visibility = Visibility.Collapsed;
            ArrowImg.Visibility = Visibility.Collapsed;
            for (int j = 0; j < App.person.Count; j++)
            {
                if (App.person[j].userEnabled == 1)
                {
                    Grid ProfileItemGrid = new Grid();
                    ProfileItemGrid.Height = 200;
                    ProfileItemGrid.Width = 200;
                    RowDefinition row1 = new RowDefinition();
                    RowDefinition row2 = new RowDefinition();
                    Button profbtns = new Button();                    
                    int currentUserTemp = j;
                    profbtns.Click += delegate
                    {
                        ProfPass.Password = "";
                        App.currentUserProfile = currentUserTemp;
                        ProfPassTxt.Visibility = Visibility.Visible;
                        ProfPassExe.Visibility = Visibility.Visible;
                        ProfPass.Visibility = Visibility.Visible;
                        ArrowImg.Visibility = Visibility.Visible;                        
                        ProfPass.Focus(FocusState.Programmatic);
                    };
                    ImageBrush imageBrush = new ImageBrush();
                    imageBrush.ImageSource = new BitmapImage(App.person[j].userPic);
                    profbtns.Background = imageBrush;
                    profbtns.Width = 150;
                    profbtns.Height = 150;
                    profbtns.Margin = new Thickness(10);
                    profbtns.HorizontalAlignment = HorizontalAlignment.Center;
                    profbtns.FontFamily = new FontFamily("Supercell-magic");
                    profbtns.FontSize = 25;
                    profbtns.PointerEntered += delegate
                    {
                        Windows.UI.Xaml.Window.Current.CoreWindow.PointerCursor = new Windows.UI.Core.CoreCursor(Windows.UI.Core.CoreCursorType.Hand, 1);
                        profbtns.Content = "Log In";
                    };
                    profbtns.PointerExited += delegate
                    {
                        Windows.UI.Xaml.Window.Current.CoreWindow.PointerCursor = new Windows.UI.Core.CoreCursor(Windows.UI.Core.CoreCursorType.Arrow, 1);
                        profbtns.Content = "";
                    };
                    TextBlock UserName = new TextBlock();
                    UserName.Text = App.person[j].userName;
                    UserName.TextAlignment = TextAlignment.Center;
                    UserName.FontFamily = new FontFamily("Supercell-magic");
                    UserName.FontSize = 25;                                   
                    ProfileItemGrid.Children.Add(UserName);
                    ProfileItemGrid.Children.Add(profbtns);
                    ProfilesPanel.HorizontalAlignment = HorizontalAlignment.Center;
                    ProfilesPanel.Children.Add(ProfileItemGrid);    
                }
            }
        }

        

        private void ProfPassExe_Click(object sender, RoutedEventArgs e)
        {
            profPassExe();
        }

        private void ProfPass_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                try
                {
                    profPassExe();
                }
                catch (FormatException)
                {
                    // Bad address.
                }
            }
        }

        public void profPassExe()
        {
            if (ProfPass.Password == App.person[App.currentUserProfile].userPassword)
            {
                App.currentUser = App.currentUserProfile;
                UserDisplay.Text = App.person[App.currentUser].userName;
                ProfPass.Password = "";
                NoUser.Visibility = Visibility.Collapsed;
                WebPage.Visibility = Visibility.Visible;
                Favorites_Btn.IsEnabled = true;
                History_Btn.IsEnabled = true;
                Settings_Btn.IsEnabled = true;
                Home_Btn.IsEnabled = true;
                Report_Btn.IsEnabled = true;
                SearchBar.Visibility = Visibility.Visible;
                Go.Visibility = Visibility.Visible;
                ArrowLeft.Visibility = Visibility.Visible;
                ArrowRight.Visibility = Visibility.Visible;
                WebViewBack.IsEnabled = true;
                WebViewForward.IsEnabled = true;
                Uri targetUri = new Uri(App.adminSettings.home);
                WebPage.Navigate(targetUri);
                ProfilesPopUp.Hide();
            }
            else
            {
                ProfPassError.Text = "Incorrect Password";
                ProfPass.Password = "";
            }
        }

        private void ProfilesGrid_Unloaded(object sender, RoutedEventArgs e)
        {
            ProfilesPanel.Children.Clear();
            ProfPassError.Text = "";
        }

        //Code for History

        private void HistoryGrid_Loaded(object sender, RoutedEventArgs e)
        {
            for (int m = 0; m < App.history.Count; m++)
            {
                var currentDate = DateTime.Now;
                var currentDateOnly = currentDate.Date;
                var instanceDate = App.history[m].date;
                var instanceDateOnly = instanceDate.Date;
                if (instanceDateOnly == currentDateOnly && App.history[m].userName == App.person[App.currentUser].userName && App.history[m].flagged == 0)
                {
                    History_Loop_Today(m);
                }
            }

            for (int n = 0; n < App.history.Count; n++)
            {
                var currentDate = DateTime.Now;
                var currentDateOnlyMin = currentDate.Date.AddDays(-1);
                var currentDateOnlyMax = currentDate.Date.AddDays(-7);
                var instanceDate = App.history[n].date;
                var instanceDateOnly = instanceDate.Date;
                if (instanceDateOnly < currentDateOnlyMin && instanceDateOnly > currentDateOnlyMax && App.history[n].userName == App.person[App.currentUser].userName && App.history[n].flagged == 0)
                {
                    History_Loop_7_Days(n);
                }
            }
        }
        public void History_Loop_Today(int q)
        {
            HyperlinkButton historyitem = new HyperlinkButton();
            historyitem.Content = App.history[q].siteUrlHis;
            historyitem.Foreground = new SolidColorBrush(Windows.UI.Colors.Black);
            string siteURL = App.history[q].siteUrlHis;
            historyitem.Click += delegate
            {
                WebNav(siteURL);
                HistoryPopUp.Hide();
            };

            HistoryToday.Items.Add(historyitem);
        }

        public void History_Loop_7_Days(int q)
        {
            HyperlinkButton historyitem = new HyperlinkButton();
            historyitem.Content = App.history[q].siteUrlHis;
            historyitem.Foreground = new SolidColorBrush(Windows.UI.Colors.Black);
            string siteURL = App.history[q].siteUrlHis;
            historyitem.Click += delegate
            {
                WebNav(siteURL);
                HistoryPopUp.Hide();
            };

            History7Days.Items.Add(historyitem);
        }
        private void HistoryGrid_Unloaded(object sender, RoutedEventArgs e)
        {
            HistoryToday.Items.Clear();
            History7Days.Items.Clear();
        }

        //Code for Favorites
        private void Grid_Loaded_Favourites(object sender, RoutedEventArgs e)
        {
            for (int l = 0; l < App.favorites.Count; l++)            {

                if (App.favorites[l].userName == App.person[App.currentUser].userName)
                {
                    int n = l;
                    Button favsbtns = new Button();
                    favsbtns.Content = App.favorites[l].siteName;
                    string siteURL = App.favorites[l].siteUrl;
                    favsbtns.FontFamily = new FontFamily("Supercell-magic");
                    favsbtns.Click += delegate
                    {
                        WebNav(siteURL);
                        FavFlyout.Hide();
                    };
                    favsbtns.RightTapped += delegate
                    {
                        MenuFlyout m = new MenuFlyout();
                        MenuFlyoutItem mn = new MenuFlyoutItem();
                        mn.FontFamily = new FontFamily("Supercell-magic");
                        mn.Text = "Delete";
                        mn.Click += delegate
                        {
                            App.connection.Open();
                            SQLiteCommand deleteUser = new SQLiteCommand("DELETE FROM Favorites WHERE SiteName = @name;", App.connection);
                            deleteUser.Parameters.AddWithValue("@name", App.favorites[n].siteName);
                            deleteUser.ExecuteNonQuery();
                            App.connection.Close();
                            App.favorites.RemoveAt(n);
                            wrap_panel_favourites.Children.RemoveAt(n);
                        };
                        m.Items.Add(mn);
                        m.ShowAt((FrameworkElement)favsbtns);
                    };
                    //ImageBrush imageBrush = new ImageBrush();
                    //imageBrush.ImageSource = new BitmapImage(new Uri(App.favourites[l].imageRef));
                    //favsbtns.Background = imageBrush;
                    favsbtns.Width = 100;
                    favsbtns.Height = 100;
                    favsbtns.Margin = new Thickness(10);
                    wrap_panel_favourites.Children.Add(favsbtns);
                }

                else
                {

                }
            }
        }

        private void AddFavBtn1_Click(object sender, RoutedEventArgs e)
        {
            if (siteName.Text == "" || siteURL.Text == "")
            {
                FavClickOutput.Text = "Name or Url not added";
            }
            else if (wrap_panel_favourites.Children.Count >= 22)
            {
                FavClickOutput.Text = "Maximum Favourites Added";
            }
            else
            {
                string siteNameTxt = siteName.Text;
                string siteUrlTxt = siteURL.Text;
                string siteImageTxt = siteURL.Text + "/favicon.ico";
                App.favorites.Add(new FavoritesData(siteNameTxt, siteUrlTxt, siteImageTxt, App.person[App.currentUser].userName));
                int k = App.favorites.Count - 1;
                App.connection.Open();
                SQLiteCommand writeFavorite = new SQLiteCommand("INSERT into Favorites(SiteName, SiteURL, ImageRef, UserName) Values(@sitename, @siteurl, @imageref, @username)", App.connection);
                writeFavorite.Parameters.AddWithValue("@sitename", App.favorites[k].siteName);
                writeFavorite.Parameters.AddWithValue("@siteurl", App.favorites[k].siteUrl);
                writeFavorite.Parameters.AddWithValue("@imageref", App.favorites[k].imageRef);
                writeFavorite.Parameters.AddWithValue("@username", App.favorites[k].userName);
                writeFavorite.ExecuteNonQuery();
                App.connection.Close();                
                Button favsbtns = new Button();
                favsbtns.Content = App.favorites[k].siteName;
                string siteURLgo = App.favorites[k].siteUrl;
                
                favsbtns.Click += delegate
                {
                    WebNav(siteURLgo);
                    FavFlyout.Hide();
                };
               
                favsbtns.Width = 100;
                favsbtns.Height = 100;
                favsbtns.Margin = new Thickness(10);

                favsbtns.RightTapped += delegate
                {
                    MenuFlyout m = new MenuFlyout();
                    MenuFlyoutItem mn = new MenuFlyoutItem();
                    mn.Text = "Delete";
                    mn.Click += delegate
                    {

                    };
                    m.Items.Add(mn);
                    m.ShowAt((FrameworkElement)favsbtns);
                };
                wrap_panel_favourites.Children.Add(favsbtns);
                siteName.Text = "";
                siteURL.Text = "";
            }
        }

              

        private void Url_Current(object sender, RoutedEventArgs e)
        {
            siteURL.Text = App.currentURL;
        }


        private void Fav_Flyout_Closed(object sender, object e)
        {
            wrap_panel_favourites.Children.Clear();
        }

        //Code for settings
        private void Grid_Loaded_Settings(object sender, RoutedEventArgs e)
        {            
            picChange.FontSize = 40;
            picChange.Text = "Choose a profile picture";
            picChange.HorizontalAlignment = HorizontalAlignment.Center;
            picChange.VerticalAlignment = VerticalAlignment.Top;
            picChange.FontFamily = new FontFamily("Supercell-magic");
            picChange.Margin = new Thickness(10);            
            for (int i = 0; i < App.profilePics.Count; i++)
            {
                Button picbtn = new Button();
                picbtn.PointerEntered += button_PointerEntered;
                picbtn.PointerExited += button_PointerExited;
                ImageBrush imageBrush = new ImageBrush();
                imageBrush.ImageSource = new BitmapImage(App.profilePics[i].profPic);
                picbtn.Background = imageBrush;
                picbtn.Width = 200;
                picbtn.Height = 200;
                picbtn.Margin = new Thickness(10);
                int current = i;
                picbtn.Click += delegate
                {
                    App.person[App.currentUser].userPic = App.profilePics[current].profPic;
                    App.connection.Open();
                    SQLiteCommand updatePerson = new SQLiteCommand("UPDATE Person SET URI=@uri WHERE Name=@name", App.connection);
                    updatePerson.Parameters.AddWithValue("@name", App.person[App.currentUser].userName);
                    updatePerson.Parameters.AddWithValue("@uri", App.person[App.currentUser].userPic);                    
                    updatePerson.ExecuteNonQuery();
                    App.connection.Close();
                    SettingsFlyout.Hide();
                };
                wrap_panel_settings.Children.Add(picbtn);
            }
        }

        private void SettingsFlyout_Closed(object sender, object e)
        {
            wrap_panel_settings.Children.Clear();
        }

        private void WebViewBack_Click(object sender, RoutedEventArgs e)
        {
            WebPage.GoBack();
        }

        private void WebViewForward_Click(object sender, RoutedEventArgs e)
        {
            WebPage.GoForward();
        }

        
    }
}
